# Waddles Android app

The application for holonomic robot control via Bluetooth was programmed within Android Studio IDE. Run the app within this IDE or build an _.apk_ file with its tools. The detailed information on the application usage can be found in [this report](https://emilia-szymanska.gitlab.io/cv/docs/6_sem/RM_projekt.pdf).
